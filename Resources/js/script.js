$(document).ready(function(){
    var dropZone = $('#upload-container');

    $('#file-csv').focus(function() {
        $('label').addClass('focus');
    })
        .focusout(function() {
            $('label').removeClass('focus');
        });


    dropZone.on('drag dragstart dragend dragover dragenter dragleave drop', function(){
        return false;
    });

    dropZone.on('dragover dragenter', function() {
        dropZone.addClass('dragover');
    });

    dropZone.on('dragleave', function(e) {
        let dx = e.pageX - dropZone.offset().left;
        let dy = e.pageY - dropZone.offset().top;
        if ((dx < 0) || (dx > dropZone.width()) || (dy < 0) || (dy > dropZone.height())) {
            dropZone.removeClass('dragover');
        }
    });

    dropZone.on('drop', function(e) {
        dropZone.removeClass('dragover');
        let files = e.originalEvent.dataTransfer.files;
        sendFiles(files);
    });

    $('#file-csv').change(function() {
        let files = this.files;
        sendFiles(files);
    });


    function sendFiles(files) {
        let maxFileSize = 5242880;
        let Data = new FormData();
        $(files).each(function(index, file) {
            //if ((file.size <= maxFileSize) && ((file.type == 'image/png') || (file.type == 'image/jpeg'))) {
                Data.append('csv', file);
            //};
        });

        $.ajax({
            url: dropZone.attr('action'),
            type: dropZone.attr('method'),
            data: Data,
            contentType: false,
            processData: false,
            success: function(data) {
                var snackbarContainer = document.querySelector('#toast');
                var data = {message: 'Импорт успешно завершён'};
                snackbarContainer.MaterialSnackbar.showSnackbar(data);
                getRows();
            }
        });
    }

    $('#table').hide();
    function getRows() {
        $.ajax({
            url: '/rows.php',
            type: 'GET',
            success: function (data) {
                let rows = jQuery.parseJSON(data);
                rows.forEach((row) => {
                    $('#table tbody').append(
                        '<tr>' +
                        `<td>${row.code}</td>` +
                        `<td>${row.name}</td>` +
                        `<td>${row.level1}</td>` +
                        `<td>${row.level2}</td>` +
                        `<td>${row.level3}</td>` +
                        `<td>${row.price}</td>` +
                        `<td>${row.price_cp}</td>` +
                        `<td>${row.quantity}</td>` +
                        `<td>${row.property_fields}</td>` +
                        `<td>${row.joint_purchases}</td>` +
                        `<td>${row.unit}</td>` +
                        `<td>${row.image}</td>` +
                        `<td>${row.is_main ? 'Да' : 'Нет'}</td>` +
                        `<td>${row.description}</td>` +
                        '</tr>'
                    );
                });
                if(rows.length){
                    $('#table').DataTable();
                    $('#table').show();
                }
            }
        });
    }
    getRows();
})
