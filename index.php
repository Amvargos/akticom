<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Загрузка документа CSV</title>
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/Resources/css/style.css">
</head>
<body>

<form id="upload-container" method="POST" action="/import.php">
    <img id="upload-image" src="/Resources/images/upload.svg">
    <div>
        <input id="file-csv" type="file" name="csv">
        <label class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" for="file-csv">Выберите .csv
            файл</label>
        <br>
        <span>или перетащите его сюда</span>
    </div>
</form>

<div id="toast" class="mdl-js-snackbar mdl-snackbar">
    <div class="mdl-snackbar__text"></div>
    <button class="mdl-snackbar__action" type="button"></button>
</div>

<table id="table">
    <thead>
        <tr>
            <th>Код</th>
            <th>Наименование</th>
            <th>Уровень 1</th>
            <th>Уровень 2</th>
            <th>Уровень 3</th>
            <th>Цена</th>
            <th>ЦенаСП</th>
            <th>Количество</th>
            <th>Поля свойств</th>
            <th>Совместные покупки</th>
            <th>Единица измерения</th>
            <th>Картинка</th>
            <th>Выводить на главной</th>
            <th>Описание</th>
        </tr>
    </thead>
    <tbody>
    
    </tbody>
</table>

<script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script defer src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="/Resources/js/script.js"></script>

</body>
</html>
