<?php

class Import
{
    private $csv;
    private $db;

    public function __construct($csv = null, $db = null)
    {
        $this->csv = $csv;
        $this->db = $db;
    }
    public function getRows(){
        $rows = $this->db->query("SELECT * FROM akticom")->fetchAll();
        return json_encode($rows);
    }

    public function start()
    {
        $filename = $this->csv["tmp_name"];
        if ($this->csv['size'] > 0) {
            $file = fopen($filename, "r");
            $i = 0;
            $sql = "INSERT INTO akticom (
                     code,
                     name,
                     level1,
                     level2,
                     level3,
                     price,
                     price_cp,
                     quantity,
                     property_fields,
                     joint_purchases,
                     unit,
                     image,
                     is_main,
                     description) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $stmt = $this->db->prepare($sql);
            try {
                $this->db->beginTransaction();
                while (($getData = fgetcsv($file, 10000, ";")) !== FALSE) {
                    if ($i > 0) {
                        $stmt->execute([
                            str_replace('"', '', $getData[0]),
                            str_replace('"', '', $getData[1]),
                            str_replace('"', '', $getData[2]),
                            str_replace('"', '', $getData[3]),
                            str_replace('"', '', $getData[4]),
                            str_replace('"', '', $getData[5]),
                            str_replace('"', '', $getData[6]),
                            str_replace('"', '', $getData[7]),
                            str_replace('"', '', $getData[8]),
                            str_replace('"', '', $getData[9]),
                            str_replace('"', '', $getData[10]),
                            str_replace('"', '', $getData[11]),
                            str_replace('"', '', $getData[12]),
                            str_replace('"', '', $getData[13])
                        ]);
                    }
                    $i++;
                }
                return $this->db->commit();
            } catch (Exception $e) {
                $this->db->rollback();
                return $e;
            }
        }
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getAge()
    {
        return $this->age;
    }
}
